import { useState, useEffect } from 'react';
import axios from 'axios'
import { SOLR } from '../../config/config.js';
/**
 * @return { { reparticiones:Reparticion[], loading:boolean, error:string | null } } 
 */
function useReparticiones() {
    const [loading, setLoading] = useState(null);
    const [error, setError] = useState(null);
    const [reparticiones, setReparticiones] = useState([])

    useEffect(() => {
        setLoading(true)
        setReparticiones([]);
        setError(null);
        const url = SOLR + "/coreREPARTICIONES/select?q=*%3A*&rows=9999&wt=json&indent=true";
        axios.get(url).then((res) => {
            console.log(res)
            const reparticiones = res?.data?.response?.docs;
          /*  const reparticiones = res?.data?.response?.docs?.filter(e =>
                !e.codigo.includes("#TSJ") &&
                !e.codigo.includes("#CM") &&
                e.codigo !== "TSJ" &&
                e.codigo !== "CM")
*/
            setReparticiones(reparticiones)

        })
            .catch((err) => {
                console.error(err)
                setError('Hubo un error al obtener las reparticiones')
            }).finally(() => setLoading(false))
    }, [])

    return { reparticiones, loading, error }
}

export default useReparticiones;

/** @typedef {import ("../../config/types.js").Reparticion} Reparticion */
