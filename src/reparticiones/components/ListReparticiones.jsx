import Form from "react-bootstrap/Form";
import PropTypes from 'prop-types'

import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import LoadingSpinner from "../../shared/components/LoadingSpinner";
import useReparticiones from "../hooks/useReparticiones";


function ListarReparticionesInputBox({ setFiltroReparticion, defaultValue }) {
    const { loading, reparticiones } = useReparticiones();

    function renderElement() {
        return reparticiones?.map((e) => ({
            id: e.codigo,
            label: e.codigo + " - " + e.nombre,
        }));
    }

    return (
        <Form.Group controlId="reparticion">
            <Form.Label >
                Reparticion Generadora
            </Form.Label>
            {loading ? (
                <LoadingSpinner />
            ) :
                <Typeahead
                    id="typeahead"
                    paginationText="Mostrar mas resultados..."
                    options={renderElement()}
                    placeholder="Ej: ADG - Administración General"
                    onChange={(e) => setFiltroReparticion(e[0]?.id || defaultValue)}
                />
            }
        </Form.Group>
    );

}


ListarReparticionesInputBox.propTypes = {
    setFiltroReparticion: PropTypes.func.isRequired,
    defaultValue: PropTypes.string.isRequired
}

export default ListarReparticionesInputBox;