import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Navigate } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import './App.css'
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead.bs5.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import PrivateRoute from './shared/components/private-route.component';
import BusquedaNavbar from './shared/components/Navbar';
import BusquedaExpediente from './expediente/components/BusquedaExpediente';
import BusquedaExpedienteActivo from './expediente/components/BusquedaExpedienteActivo';
import BusquedaExpedienteAuditoria from './expediente/components/BusquedaExpedienteAuditoria';
import SearchBox from './expediente/components/Searchbox';
import FindDocumento from './documento/components/BusquedaDocumentos';
import ConfigUrlUsuario from './login/components/ConfigUrlUsuario.component';
import ListarEmails from './email/components/ListarEmails';
import { useState } from 'react';
import DescargarDocumentoWatermark from './documento/components/DescargarDocumentoWatermark';


function App() {
	const [redirect, setRedirect] = useState(false);


	return (
		<>
			<BrowserRouter>
				<BusquedaNavbar setRedirect={setRedirect} />
				<Container>
					<Routes>
						<Route path="/ver-documento" element={<DescargarDocumentoWatermark />} >
						</Route>
						<Route
							path="/"
							element={(<>
								<SearchBox />
								<PrivateRoute redirect={redirect} setRedirect={setRedirect} />
							</>)}>
							<Route path="/" element={<Navigate to={'/gde_gedos'} />} />
							<Route path="/gde_gedos" element={<FindDocumento />} />
							<Route path="/gde_expedientes" element={<BusquedaExpediente />} />
							<Route path="/gde_expedientes/historial" element={<BusquedaExpedienteAuditoria />} />
							<Route path="/gde_expedientes/activos" element={<BusquedaExpedienteActivo />} />
							<Route path="*" element={<Navigate to={'/gde_gedos'} />} />

						</Route>

						<Route path="/" element={<PrivateRoute redirect={redirect} setRedirect={setRedirect} />}>
							<Route path="/config" element={<ConfigUrlUsuario />} />
							<Route path="/envios-externos" element={<ListarEmails />} />
						</Route>
					</Routes>
				</Container>
			</BrowserRouter>
			{/*<Footer />*/}
		</>
	)
}

export default App
