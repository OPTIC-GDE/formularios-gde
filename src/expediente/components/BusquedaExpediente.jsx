import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import "../../shared/styles/clickeableTable.css";
import ListarReparticionesInputBox from '../../reparticiones/components/ListReparticiones.jsx';
import Tabla from '../../shared/components/Tabla.jsx';
import ListTratasInputBoxMultivalued from './ListTratasInputBoxMultivalued.jsx';
import { completaZero, formatFecha2 } from '../../shared/util/helpers.js';
import axios from 'axios';
import { SOLR } from '../../config/config.js';
import DetalleExpediente from './DetalleExpediente.jsx';
import LoadingSpinner from '../../shared/components/LoadingSpinner.jsx';
import ShowMore from '../../shared/components/ShowMore.jsx';


const filtrosDefault = {
    estado: '*',
    motivo: '*',
    usuarioTramitante: '*',
    numeroExp: '*',
    anioExp: '*',
    numeroRep: '*',
    tratas: [],
    fechaDesde: '2016-12-01',
    fechaHasta: '2040-12-01'
}

/** @type {ColumnsTablaRender[]} */
const columnasConfig = [{
    style: {
        cursor: "pointer",
        width: '25%'
    },
    nombreAtributo: 'expediente',
    nombre: 'Expediente',
}, {
    nombreAtributo: 'motivo',
    nombre: 'Motivo'
}, {
    nombreAtributo: 'usuario_creador',
    nombre: 'Tramitante'
}, {
    nombreAtributo: 'assignee_ee',
    nombre: 'Usuario Actual'
}, {
    nombreAtributo: 'fecha_creacion',
    nombre: 'Fecha Creación',
}]


function BusquedaExpediente() {


    const [filtros, setFiltros] = useState({ ...filtrosDefault });
    const [disabledBoton, setDisabledBoton] = useState(false);
    const [alertaVisible, setAlertaVisible] = useState(false);
    const [mensajeAlerta, setMensajeAlerta] = useState("");
    /** @type {[Expediente[], React.Dispatch<Expediente[]>]} */
    const [datos, setDatos] = useState([]);
    const [mostrarDetalle, setMostrarDetalle] = useState(false);
    const [detalle, setDetalle] = useState(null);

    const numeroRegistros = 600;

    /**
    *
    * @return {TablaElementoExpediente[]} 
    */
    function formatearFilas() {
        return datos.map(expediente => {
            const { assignee } = expediente
            const numeroExp = completaZero("00000000" + expediente.numero, 8);
            const nroExpediente = expediente.tipo_documento + '-' + expediente.anio + '-' + numeroExp + '--' + expediente.codigo_reparticion_actuacion + '-' + expediente.codigo_reparticion_usuario;
            let usuarioActual = assignee ? assignee[assignee.length - 1] : 'ARCHIVADO';
            return {
                id: nroExpediente,
                expediente: <div className='tablas-element-row' onClick={() => {
                    setDetalle(expediente);
                    setMostrarDetalle(true);
                }}>{nroExpediente}</div>,
                id_documento: expediente.id_documento,
                motivo: <ShowMore text={expediente.descripcion} />,
                usuario_creador: expediente.usuario_creador,
                assignee_ee: usuarioActual,
                fecha_creacion: formatFecha2(expediente.fecha_creacion),
            }
        })
    }

    const onCambioHandler = (nuevoValor, filtro) => {
        let valor = nuevoValor?.target?.value
        valor = valor && valor !== '' ? valor : filtrosDefault[filtro]
        setFiltros({
            ...filtros,
            [filtro]: valor
        })
    }

    const onBuscarHandler = (e) => {
        e.preventDefault(); //prevenimos el comportamiento por defecto del boton en un formulario
        setDisabledBoton(true);
        setDatos([])

        // const selectCol = '&fl=id_documento,tipo_documento,usuario_creador,codigo_reparticion_usuario,codigo_reparticion_actuacion,descripcion,numero,anio,fecha_creacion,tramitante,codigo_reparticion_destino,assignee_ee'

        let filtroRep = "codigo_reparticion_usuario: *"
            + encodeURIComponent(filtros.numeroRep) + "*";

        let filtroMotivo = filtros.motivo === '*' ? '&q=*:*' : `&q=descripcion:"${filtros.motivo}"`;
        const filtroUsuario = filtros.usuarioTramitante !== '*' ? ("&fq=usuario_creador:" + filtros.usuarioTramitante.toUpperCase()) : '';
        let filtroFec = "fecha_creacion: [" + filtros.fechaDesde
            + "T00:00:00Z TO " + filtros.fechaHasta + "T23:59:59Z ]";
        let filtroNum = ``;
        let filtroAnio = ``;


        if (filtros.numeroExp != '*') filtroNum = `&fq=numero:${Number(filtros.numeroExp)}`
        if (filtros.anioExp != '*') filtroAnio = `&fq=anio:${Number(filtros.anioExp)}`
        let filtrosTratas = ''
        if (filtros.tratas?.length > 0) {
            filtrosTratas = `&fq=codigo_trata:(${filtros.tratas.join(" OR ")})`
        }
        // let noTribunal = "-codigo_reparticion_usuario:DGI%23TSJ";
        let filtroEstado = ``;
        if (filtros.estado !== '*') {
            switch (filtros.estado) {
                case "1":
                    filtroEstado = `&fq=-estado:(Guarda Temporal)&fq=assignee_ee:*`;
                    break;
                case "2":
                    filtroEstado = `&fq=estado:(Guarda Temporal) OR filter(-assignee_ee:*)`;
                    break;
                default:
                    break;
            }
        }

        let url = filtroMotivo + filtroNum + filtrosTratas + filtroUsuario + filtroAnio + filtroEstado + '&fq=' + filtroRep + '&fq=' + filtroFec + '&fq=' /*+ noTribunal */
            + '&version=2.2&start=0&rows=' + numeroRegistros + '&indent=on&wt=json&sort=fecha_creacion+desc';

        axios.get(SOLR + '/core-ee/select?' + url)
            .then(res => {
                if (res.data?.response?.docs?.length) {

                    setDatos(res.data?.response.docs);
                    setAlertaVisible(false);
                } else {
                    setAlertaVisible(true);
                    setMensajeAlerta('Su consulta no devolvio datos!');
                }

            })
            .catch(err => {
                console.log(err);
                setAlertaVisible(true);
                setMensajeAlerta('Hubo un error al consultar el sistema de expedientes');
            })
            .finally(() => setDisabledBoton(false));

    }

    function formulario() {

        return (
            <Form style={{ paddingTop: '15px' }}>

                <Row>
                    <Form.Group as={Col} controlId="numero">
                        <Form.Label >Número Expediente</Form.Label>
                        <Form.Control
                            name='numero'
                            placeholder="Ej: 35"
                            onChange={(campo) => onCambioHandler(campo, 'numeroExp')} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="anio">
                        <Form.Label >Año del expediente</Form.Label>
                        <Form.Control
                            name='anio'
                            placeholder="Ej: 2024"
                            onChange={(campo) => onCambioHandler(campo, 'anioExp')} />
                    </Form.Group>
                </Row>

                <Row>
                    <Form.Group as={Col} xs controlId="usuario">
                        <Form.Label >Usuario creador</Form.Label>
                        <Form.Control
                            name='usuario'
                            placeholder="Ej: MGIGLIO"
                            onChange={(campo) => onCambioHandler(campo, 'usuarioTramitante')} />
                    </Form.Group>

                    <Form.Group as={Col} xs={3} controlId="estado">
                        <Form.Label>Estado</Form.Label>
                        <Form.Control
                            as="select" name='estado' aria-label="estado" onChange={(campo) => onCambioHandler(campo, 'estado')}>
                            <option>Todos</option>
                            <option value="1">Activo</option>
                            <option value="2">Finalizado</option>
                        </Form.Control>
                    </Form.Group>
                </Row>

                <Row>
                    <ListarReparticionesInputBox
                        setFiltroReparticion={(repId) => setFiltros(prev => ({ ...prev, numeroRep: repId }))}
                        defaultValue={filtrosDefault.numeroRep}
                    />

                </Row>

                <Row>
                    <ListTratasInputBoxMultivalued
                        name="tratas"
                        setFiltros={setFiltros} />
                </Row>

                <Row>
                    <Form.Group as={Col} controlId="fechaDesde">
                        <Form.Label >Fecha Desde</Form.Label>
                        <Form.Control
                            name='fechaDesde'
                            type="date"
                            placeholder="Fecha Desde..."
                            onChange={(campo) => onCambioHandler(campo, 'fechaDesde')} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="fechaHasta">
                        <Form.Label >Fecha Hasta</Form.Label>
                        <Form.Control
                            name='fechaHasta'
                            type="date"
                            placeholder="Fecha Hasta..."
                            onChange={(campo) => onCambioHandler(campo, 'fechaHasta')} />
                    </Form.Group>
                </Row>

                <Row>
                    <Form.Group as={Col} controlId="motivo">
                        <Form.Label >Motivo</Form.Label>
                        <Form.Control
                            name='motivo'
                            as='textarea'
                            rows="2"
                            placeholder="Ej: Expediente principal de pagos"
                            onChange={(campo) => onCambioHandler(campo, 'motivo')} />
                    </Form.Group>
                </Row>

                <Button
                    className="mt-3 mb-3"
                    variant="primary"
                    type="submit"
                    md={2}
                    disabled={disabledBoton}
                    onClick={onBuscarHandler}>
                    Buscar {disabledBoton ? <LoadingSpinner /> : null}
                </Button>
            </Form>

        )
    }

    return (
        <>  {detalle ?
            <DetalleExpediente
                contenidoModal={detalle}
                mostrarModal={mostrarDetalle}
                cerrarModal={() => setMostrarDetalle(false)}
            /> : <></>}
            <Container className="shadow-lg" style={{ backgroundColor: '#e2e2e2', width: "800px" }}>

                <h4 style={{ color: 'blue' }}>Busca expedientes de acuerdo a los filtros que se ingresen</h4>

                {formulario()}


            </Container>
            <Container style={{ paddingTop: "15px" }}>
                <Alert style={{ width: "600px" }} hidden={!alertaVisible} variant='danger'>
                    {mensajeAlerta}
                </Alert>

                {datos?.length > 0 ? (<Tabla data={formatearFilas(datos)} columnsName={columnasConfig} />) : null}

            </Container>
        </>
    )


}

export default BusquedaExpediente;

/** @typedef {import ("../../config/types.js").ColumnsTablaRender} ColumnsTablaRender */
/** @typedef {import ("../../config/types.js").Expediente} Expediente */
/** @typedef {import ("../../config/types.js").TablaElementoExpediente} TablaElementoExpediente */
