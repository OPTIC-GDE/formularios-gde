import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import { Link, useHref } from 'react-router-dom';

const SearchBox = () => {
  const ref = useHref();
  return (
    <>
      <Container style={{ width: "824px" }}>

        <Nav variant="tabs" defaultActiveKey={ref} style={{ backgroundColor: '#e2e2e2' }}>
          <Nav.Item>
            <Nav.Link eventKey={"/gde_gedos"} as={Link} to="/gde_gedos">Documentos</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={"/gde_expedientes"} as={Link} to="/gde_expedientes">Expedientes</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={"/gde_expedientes/historial"} as={Link} to="/gde_expedientes/historial">Historial de consultas</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={'/gde_expedientes/activos'} as={Link} to="/gde_expedientes/activos">Expedientes de activos</Nav.Link>
          </Nav.Item>

        </Nav>
      </Container >
    </>


  )
}

export default SearchBox;
