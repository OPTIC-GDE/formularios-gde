import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { completaZero } from '../../shared/util/helpers';
import Tabla from '../../shared/components/Tabla';
import PropTypes from 'prop-types'
import ShowMore from '../../shared/components/ShowMore.jsx';

/** @type {ColumnsTablaRender[]} */
const columnasHistorialConfig = [
    {
        nombreAtributo: 'fechaOperacion',
        nombre: 'Fecha del pase'
    }, {
        nombreAtributo: 'asignado',
        nombre: 'Paso por'
    }, {
        nombreAtributo: 'reparticionDestino',
        nombre: 'Reparticion destino'
    }, {
        nombreAtributo: 'motivo',
        nombre: 'Motivo de pase',
    }]


/**
 * @param {DetalleModalExpediente}
 */
function DetalleExpediente({ contenidoModal, cerrarModal, mostrarModal }) {
    const {assignee} = contenidoModal;
    let expediente = contenidoModal.tipo_documento + '-' + contenidoModal.anio + '-' + completaZero("00000000" + contenidoModal.numero, 8) + '--' + contenidoModal.codigo_reparticion_actuacion + '-' + contenidoModal.codigo_reparticion_usuario
    let usuarioActual = assignee ? assignee[assignee.length - 1] : 'ARCHIVADO';


    function contentData() {
        return <Table size='sm' striped responsive id='tabla-modal'>
            <tbody>
                <tr><td width='30%'>AÑO:</td><td>{contenidoModal.anio}</td></tr>
                <tr><td width='30%'>TRATA:</td><td>{`${contenidoModal.codigo_trata} - ${contenidoModal.trata_descripcion}`}</td></tr>
                <tr><td width='30%'>DESCRIPCION:</td><td>{contenidoModal.descripcion}</td></tr>
                <tr><td width='30%'>SOLICITANTE:</td><td>{contenidoModal.solicitante}</td></tr>
                <tr><td width='30%'>APELLIDO SOLICITANTE:</td><td>{contenidoModal.apellido_solicitante}</td></tr>
                <tr><td width='30%'>CUIT/CUIL SOLICITANTE:</td><td>{contenidoModal.cuit_solicitante}</td></tr>
                <tr><td width='30%'>ESTADO:</td><td>{contenidoModal.estado}</td></tr>
                <tr><td width='30%'>FECHA CREACION:</td><td>{(new Date(contenidoModal.fecha_creacion)).toLocaleString("es-AR")}</td></tr>
                <tr><td width='30%'>CUIT/CUIL:</td><td>{contenidoModal.cuit}</td></tr>
                <tr><td width='30%'>USUARIO CREADOR:</td><td>{contenidoModal.usuario_creador}</td></tr>
                <tr><td width='30%'>USUARIO TENENCIA:</td><td>{usuarioActual}</td></tr>
                <tr><td width='30%'>REPARTICION:</td><td>{contenidoModal.codigo_reparticion_usuario}</td></tr>
                <tr><td width='30%'>DEPARTAMENTO:</td><td>{contenidoModal.departamento}</td></tr>
                <tr><td width='30%'>DOMICILIO:</td><td>{contenidoModal.domicilio}</td></tr>
                <tr><td width='30%'>CODIGO POSTAL:</td><td>{contenidoModal.codigo_postal}</td></tr>
                <tr><td width='30%'>EMAIL:</td><td>{contenidoModal.EMAIL}</td></tr>
                <tr><td width='30%'>MOTIVO:</td><td>{contenidoModal.motivo}</td></tr>
            </tbody>
        </Table>;
    }

    function generateHistorial() {
        let historial = contenidoModal?.assignee?.map((e, i) => {
            let reparticionDestino = contenidoModal.codigo_reparticion_destino[i];
            if (reparticionDestino === '0') reparticionDestino = null;
            return {
                asignado: e,
                fechaOperacion: (new Date(contenidoModal.fecha_operacion[i])).toLocaleString("es-AR"),
                motivo: <ShowMore text={contenidoModal.motivo_historial[i]} />,
                reparticionDestino
            }
        })
        if (!historial) historial = [];
        historial = historial?.sort((a, b) => a.fechaOperacion < b.fechaOperacion);
        return historial;
    }
    return (
        <Modal size='lg' show={mostrarModal} onHide={cerrarModal}>
            <Modal.Header closeButton>
                <Modal.Title>Expediente {expediente}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Tabs defaultActiveKey="datos" id="uncontrolled-tab-example">
                    <Tab eventKey="datos" title="Datos">
                        {contentData()}
                    </Tab>
                    <Tab eventKey="historial" title="Historial">
                        <Tabla
                            columnsName={columnasHistorialConfig}
                            data={generateHistorial()}
                        />
                    </Tab>

                </Tabs>
            </Modal.Body>
        </Modal>
    )
}


DetalleExpediente.propTypes = {
    contenidoModal: PropTypes.object,
    cerrarModal: PropTypes.func,
    mostrarModal: PropTypes.bool.isRequired
}

export default DetalleExpediente;

/** @typedef {import ("../../config/types.js").DetalleModalExpediente} DetalleModalExpediente */
/** @typedef {import ("../../config/types.js").Expediente} Expediente */
