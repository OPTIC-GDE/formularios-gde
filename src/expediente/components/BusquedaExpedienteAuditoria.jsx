import { useState } from 'react';
import Container  from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import { SOLR } from '../../config/config';
import axios from 'axios';
import ListarReparticionesInputBox from '../../reparticiones/components/ListReparticiones';
import Tabla from '../../shared/components/Tabla';
import LoadingSpinner from '../../shared/components/LoadingSpinner';
import { completaZero, formatFecha2 } from '../../shared/util/helpers';

const columnasConfig = [{
    nombreAtributo: 'expediente',
    nombre: 'Expediente'
}, {
    nombreAtributo: 'fechaConsulta',
    nombre: 'Fecha de consulta'
},
{
    nombreAtributo: 'usuario',
    nombre: 'Usuario'
}]

function BusquedaExpedienteAuditoria() {
    const numeroRegistros = 300;
    const filtrosDefault = {
        usuarioAuditado: "*",
        numeroExp: "*",
        numeroRep: "*",
        fechaDesde: '2021-12-01',
        fechaHasta: '2040-12-01'
    }

    const [filtros, setFiltros] = useState({ ...filtrosDefault });

    const [disabledBoton, setDisabledBoton] = useState(false);
    const [alertaVisible, setAlertaVisible] = useState(false);
    const [mensajeAlerta, setMensajeAlerta] = useState("");

    const [tablaVisible, setTablaVisible] = useState(false);
    const [datos, setDatos] = useState([]);


    const onCambioHandler = (nuevoValor, filtro) => {
        let valor = nuevoValor?.target?.value
        valor = valor && valor !== '' ? valor : filtrosDefault[filtro]
        setFiltros({
            ...filtros,
            [filtro]: valor
        })
    }

    function formatearFilas() {
        return datos.map(expediente => {
            const numeroExp = completaZero("00000000" + expediente.numero_actuacion, 8);
            const nroExpediente = expediente.tipo_actuacion + '-' + expediente.anio_actuacion + '-' + numeroExp + '--' + expediente.reparticion_actuacion + '-' + expediente.reparticion_usuario;
            return {
                id: nroExpediente,
                expediente: nroExpediente,
                usuario: expediente.usuario,
                fechaConsulta: formatFecha2(expediente.fecha_consulta),
            }
        })
    }

    function formulario() {

        return (
            <Form style={{ paddingTop: '15px' }}>
                <Row>

                    <Form.Group as={Col} xs controlId="numero">
                        <Form.Label >Número Expediente</Form.Label>
                        <Form.Control
                            placeholder="Ingrese número a buscar..."
                            onChange={(campo) => onCambioHandler(campo, 'numeroExp')} />
                    </Form.Group>

                    <Form.Group as={Col} xs controlId="usuario">
                        <Form.Label >Usuario consultante</Form.Label>
                        <Form.Control
                            placeholder="Ingrese nombre de usuario..."
                            onChange={(campo) => onCambioHandler(campo, 'usuarioAuditado')} />
                    </Form.Group>


                </Row>
                <Row>

                    <ListarReparticionesInputBox
                        defaultValue={filtrosDefault.numeroRep}
                        setFiltroReparticion={(rep) => setFiltros((prev) => ({ ...prev, numeroRep: rep }))}
                    />
                </Row>

                <Row>
                    <Form.Group as={Col} controlId="fechaDesde">
                        <Form.Label >Fecha consulta desde</Form.Label>
                        <Form.Control
                            type="date"
                            placeholder="Fecha Desde..."
                            onChange={(campo) => onCambioHandler(campo, 'fechaDesde')} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="fechaHasta">
                        <Form.Label >Fecha consulta hasta</Form.Label>
                        <Form.Control
                            type="date"
                            placeholder="Fecha Hasta..."
                            onChange={(campo) => onCambioHandler(campo, 'fechaHasta')} />
                    </Form.Group>
                </Row>
                <Button
                    className="mt-3 mb-3"
                    variant="primary"
                    type="submit"
                    md={2}
                    disabled={disabledBoton}
                    onClick={onBuscarHandler}>
                    Buscar {disabledBoton ? <LoadingSpinner/> : null}
                </Button>
            </Form>

        )
    }

    const onBuscarHandler = (e) => {
        e.preventDefault(); //prevenimos el comportamiento por defecto del boton en un formulario
        setDisabledBoton(true);
        setDatos([]);
        setTablaVisible(false);
        const usuariofiltro = `&q=usuario:${filtros.usuarioAuditado.toLocaleUpperCase()}`;
        const reparticionFiltro = `&fq=reparticion_usuario:${filtros.numeroRep}`;
        let anioFiltro = ""
        let numeroFiltro = "";
        if (filtros.numeroExp !== "*") {
            let [, anioExpediente, numeroExpediente] = filtros.numeroExp.split("-");
            anioFiltro = `&fq=anio_actuacion:${anioExpediente}`;
            numeroFiltro = `&fq=numero_actuacion:${Number(numeroExpediente)}`;
        }
        let filtroFec = "&fq=fecha_consulta: [" + filtros.fechaDesde
            + "T00:00:00Z TO " + filtros.fechaHasta + "T23:59:59Z ]";

        let url = SOLR + '/coreAUDITORIA/select?' + usuariofiltro + filtroFec + reparticionFiltro + anioFiltro + numeroFiltro
            + '&version=2.2&start=0&rows=' + numeroRegistros + '&indent=on&wt=json&sort=fecha_consulta+desc';

        axios.get(url)
            .then((res) => {
                if (res.data.response.docs.length) {
                    const datos = res.data.response.docs;
                    setDatos(datos);
                    setTablaVisible(true);
                    setAlertaVisible(false);
                } else {
                    setTablaVisible(false);
                    setAlertaVisible(true);
                    setMensajeAlerta('Su consulta no devolvio datos!');
                }

            })
            .catch((err) => {
                console.log(err);
                setAlertaVisible(true);
                setMensajeAlerta('Hubo un error al consultar el sistema de expedientes');

            })
            .finally(() => setDisabledBoton(false));

    }

    return (
        <>
            <Container fluid style={{ backgroundColor: '#e2e2e2', paddingTop: '10px', width: "800px" }}>
                <h4 style={{ color: 'blue' }}>Lista las consultas que se realizaron a los expedientes de acuerdo al filtro ingresado</h4>
                {formulario()}
            </Container>
            <Container style={{ width: "600px", paddingTop: "5px" }}>

                <Alert style={{ width: "600px" }} hidden={!alertaVisible} variant='danger'>
                    {mensajeAlerta}
                </Alert>

            </Container>

            {tablaVisible ? (<Tabla
                columnsName={columnasConfig}
                data={formatearFilas()} />) : null}
        </>
    )


}

export default BusquedaExpedienteAuditoria;