import { useState } from 'react';
import Container  from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Row from 'react-bootstrap/Row';
import axios from 'axios';

import Tabla from '../../shared/components/Tabla';
import { formatFecha, MENSAJE_ERROR } from '../../shared/util/helpers';
import ListarReparticionesInputBox from '../../reparticiones/components/ListReparticiones';
import { urlExpedientesActivos } from '../../config/config';
import LoadingSpinner from '../../shared/components/LoadingSpinner';
import ShowMore from '../../shared/components/ShowMore';


const columnasConfig = [{
    nombreAtributo: 'nroSade',
    nombre: 'Expediente'
}, {
    nombreAtributo: 'fechaOperacion',
    nombre: 'Fecha de operacion'
},
{
    nombreAtributo: 'destinoActual',
    nombre: 'ubicacion actual'
}, {
    nombreAtributo: 'motivo',
    nombre: 'Motivo',
    style:{
        width:'50%'
    }
}]

function BusquedaExpedienteHistorial() {
    const filtrosDefault = {
        ultimo: true,
        reparticionOrigen: null,
        reparticionHistorial: null,
        fechaDesde: '01/12/2016',
        fechaHasta: '01/12/2040'
    }

    const [filtros, setFiltros] = useState({ ...filtrosDefault });
    const [disabledBoton, setDisabledBoton] = useState(false);
    const [alertaVisible, setAlertaVisible] = useState(false);
    const [datos, setDatos] = useState(null);


    function formatearFilas() {
        return datos.expedientes.map(expediente => {
            return {
                id: expediente.nroSade,
                nroSade: expediente.nroSade,
                fechaOperacion: formatFecha(expediente.fechaOperacion),
                destinoActual: expediente.destinoActual,
                motivo: <ShowMore text={expediente.motivo} /> ,
            }
        })
    }
    const onCambioHandler = (nuevoValor, filtro) => {
        let valor = nuevoValor?.target?.value
        if (filtro === 'fechaDesde' || filtro === 'fechaHasta') {
            let [anio, mes, dia] = valor.split("-");
            valor = `${dia}/${mes}/${anio}`
        }

        valor = valor && valor !== '' ? valor : filtrosDefault[filtro]
        setFiltros({
            ...filtros,
            [filtro]: valor
        })
    }

    function formulario() {

        return (
            <Form style={{ paddingTop: '15px' }}>

                <Row>
                    <ListarReparticionesInputBox
                        defaultValue='*'
                        setFiltroReparticion={(value) => setFiltros(prev => ({ ...prev, reparticionOrigen: value }))}
                    />
                </Row>

                <Row>
                    <Form.Group as={Col} xs={5} controlId="reparticionHistorial">
                        <Form.Label >Area de tramitacion</Form.Label>
                        <Form.Control
                            placeholder="ej: OPT"
                            onChange={(campo) => onCambioHandler(campo, 'reparticionHistorial')} />
                    </Form.Group>



                    <Form.Group as={Col} className="mt-4" controlId="reparticionHistorial">
                    </Form.Group>

                </Row>

                <Row>
                    <Form.Group as={Col} controlId="fechaDesde">
                        <Form.Label >Fecha operacion desde</Form.Label>
                        <Form.Control
                            type="date"
                            onChange={(campo) => onCambioHandler(campo, 'fechaDesde')} />
                    </Form.Group>

                    <Form.Group as={Col} controlId="fechaHasta">
                        <Form.Label >Fecha operacion hasta</Form.Label>
                        <Form.Control
                            type="date"
                            onChange={(campo) => onCambioHandler(campo, 'fechaHasta')} />
                    </Form.Group>
                </Row>


                <Button
                    className="mt-3 mb-3"
                    variant="primary"
                    type="submit"
                    md={2}
                    disabled={disabledBoton}
                    onClick={onBuscarHandler}>
                    Buscar {disabledBoton ? <LoadingSpinner/> : null}
                </Button>
            </Form>

        )
    }



    const onBuscarHandler = (e) => {
        e.preventDefault(); //prevenimos el comportamiento por defecto del boton en un formulario
        setDisabledBoton(true);
        setAlertaVisible(false);
        setDatos([]);
        let query = '?ambiente=prod&offset=0';
        if (filtros.reparticionHistorial) {
            query = `${query}&areaTramitacionActual=${encodeURIComponent('%' + filtros.reparticionHistorial + '%')}`
        }
        if (filtros.reparticionOrigen) {
            query = `${query}&reparticionOrigen=${encodeURIComponent(filtros.reparticionOrigen)}`
        }
        if (filtros.fechaDesde) {
            query = `${query}&fechaDesde=${filtros.fechaDesde}`
        }

        if (filtros.fechaHasta) {
            query = `${query}&fechaHasta=${filtros.fechaHasta}`
        }

        axios.get(urlExpedientesActivos + query)
            .then(res => setDatos(res.data))
            .catch(() => setAlertaVisible(true))
            .finally(() => setDisabledBoton(false))

    }
    return (
        <>
			<Container fluid style={{ backgroundColor: '#e2e2e2', paddingTop: '10px', width: "800px" }}>

                <h4 style={{ color: 'blue' }}>Buscar expedientes activos en una repartición o área</h4>

                {formulario()}

            </Container>
            <Container style={{ width: "600px", paddingTop: "5px" }}>

                <Alert style={{ width: "600px" }} hidden={!alertaVisible} variant='danger'>
                    {MENSAJE_ERROR.SERVER_ERROR}
                </Alert>

                <Alert style={{ width: "600px" }} hidden={datos?.cantidadTotal !== 0} variant='warning'>
                    {MENSAJE_ERROR.NO_HAY_EXPEDIENTE}
                </Alert>

                <Alert style={{ width: "600px" }} hidden={!(datos?.cantidadTotal > 300)} variant='warning'>
                    {MENSAJE_ERROR.MAX_CANTIDAD_DATOS + datos?.cantidadTotal}
                </Alert>

            </Container>
            {datos?.expedientes?.length > 0 ? (<Tabla
                columnsName={columnasConfig}
                data={formatearFilas()} />) : null}
        </>
    )


}

export default BusquedaExpedienteHistorial;