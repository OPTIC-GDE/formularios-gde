import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import useTratas from "../hooks/useTratas";
import PropTypes from 'prop-types'
import LoadingSpinner from "../../shared/components/LoadingSpinner";


function ListTratasInputBoxMultivalued({ setFiltros }) {
    const { tratas, loading } = useTratas()

    function onCambioTipo(nuevoValor) {
        let valor = [];
        for (let trata of nuevoValor) {
            valor.push(trata?.id);
        }
        setFiltros(prev => ({ ...prev, tratas: valor }))
    }

    function renderElement() {
        return tratas.map((resp) => ({
            id: resp.codigo_trata,
            label: (resp.trata_descripcion || 'Expediente') + " - " + resp.codigo_trata
        }));
    }



    return (
        <Form.Group as={Col} controlId="tipo">
            <Form.Label >Tramites</Form.Label>
            {loading || !tratas ? (
                <LoadingSpinner />
            ) :
                <Typeahead
                    multiple
                    id="typeahead"
                    name='trata'
                    options={renderElement()}
                    placeholder="Ej: LOCACIONES, LIQHABERES"
                    onChange={(e) => onCambioTipo(e)}
                />}
        </Form.Group>)
}
ListTratasInputBoxMultivalued.propTypes = {
    setFiltros: PropTypes.func.isRequired,
}

export default ListTratasInputBoxMultivalued;