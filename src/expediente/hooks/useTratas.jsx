import { useState, useEffect } from 'react';
import axios from 'axios'
import { SOLR } from '../../config/config.js';
/**
 * @return { { tratas:Trata[], loading:boolean, error:string | null } } 
 */
function useTratas() {
    const [loading, setLoading] = useState(null);
    const [error, setError] = useState(null);
    const [tratas, setTratas] = useState([])

    useEffect(() => {
        setLoading(true)
        setTratas(null);
        setError(null);
        const url = SOLR + "/core-ee/select?q=*%3A*&fl=codigo_trata%2Ctrata_descripcion&group=true&group.field=codigo_trata&group.main=true&wt=json&indent=true&rows=99999999";
        axios.get(url).then((res) => {
            setTratas(res?.data?.response?.docs)
        })
            .catch((err) => {
                console.error(err)
                setError('Hubo un error al obtener las tratas')
            }).finally(() => setLoading(false))
    }, [])

    return { tratas, loading, error }
}

export default useTratas;

/** @typedef {import ("../../config/types.js").Trata} Trata */
