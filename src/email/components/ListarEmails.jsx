import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import Row from 'react-bootstrap/Row'
import useFetch from "../../shared/hooks/useFetch";
import useFetchPost from "../../shared/hooks/useFetchPost";
import { URL_NOTIFICAR_COMUNICADOS, URL_OBTENER_COMUNICADOS } from "../../config/config.js";
import LoadingSpinner from "../../shared/components/LoadingSpinner";
import TableEmails from "./ListarEmailsTable";


export default function ListarEmails() {
    const [token] = useOutletContext();
    const { data, error, loading, setRefetch, refetch } = useFetch({ url: URL_OBTENER_COMUNICADOS, token });
    const { postData: postEmails, loading: loadingPostData } = useFetchPost({ url: URL_NOTIFICAR_COMUNICADOS, token })
    const [emailsEnviados, setEmailsEnviados] = useState([]);
    const [mutateData, setMutateData] = useState([])
    /** @type {{array:Destinatario[]}} */
    const [listaDestinatarios, setListaDestinatario] = useState([])

    const sendEmails = () => {
        postEmails(listaDestinatarios).then(() => {
            setRefetch(refetch + 1)
            setEmailsEnviados(listaDestinatarios.map(e => e.reparticionDestino))
            setListaDestinatario([]);
        })
    }

    /**
* @param {SyntheticBaseEvent} e event 
* @param {Destinatario} destinatario  I
**/
    const chkClick = (e, destinatario) => {
        setMutateData(mutateData.map(d => {
            if (d.id === destinatario.id) {
                return ({ ...d, enviar: !d.enviar })
            } else {
                return { ...d };
            }
        }))

        if (!(listaDestinatarios.find(d => d.id === destinatario.id))) {
            setListaDestinatario([...listaDestinatarios, destinatario])
            listaDestinatarios.push(destinatario)
        } else {
            setListaDestinatario(listaDestinatarios.filter(d => d.id !== destinatario.id))
        }
    }
    useEffect(() => {
        if (data && Array.isArray(data)) setMutateData(data.map(e => ({ ...e, enviar: false })));
        else setMutateData([])
    }, [data]);


    if (loading) return <LoadingSpinner />
    if (error) console.log(error)
    if (data && mutateData) {
        return (
            <>
                {emailsEnviados.length > 0 ?
                    <Alert key='notificacion mails' variant='success'>
                        Emails enviados a: {emailsEnviados.join(', ')}
                    </Alert>
                    :
                    null
                }
                <Tabs
                    defaultActiveKey="noEnviado"
                    id="tabs-id"
                    className="mb-3 tab">
                    <Tab eventKey="noEnviado" title="Pendientes" className="tab">
                        <TableEmails onCheck={chkClick} isEnviados={false} data={mutateData.filter(e => !e.leido)} listaDestinatarios={listaDestinatarios} />
                        <Row>
                            <Col>
                                <Button variant="success" disabled={loadingPostData} type="button" onClick={sendEmails} >
                                    Enviar emails {loadingPostData ? <LoadingSpinner /> : null}
                                </Button>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab className="tab" eventKey="enviados" title="Ya enviados">
                        <TableEmails data={mutateData.filter(e => e.leido)} isEnviados={true} />
                    </Tab>
                </Tabs>

            </>
        )
    }

}



/**
 * @typedef {import ("../../config/types.js").Destinatario} Destinatario
 * @typedef {import ("../../config/types.js").UseArray} UseArray
 */