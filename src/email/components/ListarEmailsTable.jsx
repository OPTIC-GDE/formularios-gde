import { useState, useEffect } from "react";
import Table from 'react-bootstrap/Table'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import PropTypes from "prop-types";
import Paginacion from "../../shared/components/Paginacion";


/**
 *
 * @param  {Object} options
 * @param  {Destinatario[]} options.data informacion sobre las comunicaciones pendientes del la reparticion del usuario
 * @param  {func} options.onCheck informacion sobre las comunicaciones pendientes del la reparticion del usuario
 * @param  {Boolean} options.isEnviado booleano que indetifica el tipo de componentes enviados o no enviados
 * @return {*} 
 */
function TableEmails({ data, isEnviados, onCheck }) {
    const [paginaActiva, setPaginaActiva] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(1);
    const [datosVisibles, setDatosVisibles] = useState([]);

    const ELEMENTOS_POR_PAGINA = 10;

    useEffect(() => {
        setCantidadPaginas(Math.ceil(data.length / ELEMENTOS_POR_PAGINA));
    }, [data]);


    useEffect(() => {
        let primerElemento = (paginaActiva - 1) * ELEMENTOS_POR_PAGINA;
        let ultimoElemento = paginaActiva * ELEMENTOS_POR_PAGINA;
        if (ultimoElemento > data.length) {
            ultimoElemento = data.length;
        }
        const elementosVisiblesAux = [];
        for (let i = primerElemento; i < ultimoElemento; i++) {
            elementosVisiblesAux.push(data[i]);
        }
        setDatosVisibles(elementosVisiblesAux);
    }, [paginaActiva, data]);



    function Tabla() {
        return (<Table className="tablas-element" responsive="sm">
            <thead>
                <tr>
                    <th>NroSade</th>
                    <th>Destinatario</th>
                    <th>Email destino</th>
                    {isEnviados ? <th>Fecha enviado</th> : <th>Enviar</th>}
                </tr>
            </thead>
            <tbody>
                {datosVisibles.map(e => (
                    <tr className="tablas-element-row" key={e.id}>
                        <td>{e.nroSade}</td>
                        <td>{e.nombreUsuario}</td>
                        <td>{e.reparticionDestino}</td>

                        <td>{!e.leido ? <Form.Check
                            type='checkbox'
                            onClick={(event) => onCheck(event, e)}
                            checked={e.enviar}
                            id={`${e.id}`}
                        /> : (new Date(e.leido)).toLocaleString("es-ES")} </td>
                    </tr>))
                }
            </tbody>
        </Table>)
    }


    return (
        <>
            <Row>
                <Tabla />
            </Row>

            <Row className="justify-content-md-center">
                <Col md={2}>
                    <Paginacion
                        paginaActiva={paginaActiva}
                        setPaginaActiva={setPaginaActiva}
                        cantidadPaginas={cantidadPaginas}
                    />
                </Col>
            </Row>
        </>
    )


}


TableEmails.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        reparticionDestino: PropTypes.string,
        mesaExterna: PropTypes.string,
        nroSade: PropTypes.string,
        leido: PropTypes.string
    })),
    onCheck: PropTypes.func,
    isEnviados: PropTypes.bool
}

export default TableEmails;

/**
 *
 * @typedef {import ("../../config/types.js").Destinatario} Destinatario
 * @typedef {import ("../../config/types.js").UseArray} UseArray
 */