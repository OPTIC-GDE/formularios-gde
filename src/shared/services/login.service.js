import { IS_LOGIN } from "../../config/config"
import axios from 'axios';

export const isLogin = async (tokenRecibido) => {
    try {
        const token = localStorage.getItem("token");
        let res  = await chkToken(token) || await chkToken(tokenRecibido)
        return res
    } catch (e) {
        return false
    }

}

async function chkToken(token) {
    try {
        const resp = await axios.get(IS_LOGIN, {
            headers: {
                'Content-Type':"application/json",
                'Authorization': token
            }
        })
        localStorage.setItem('token', resp.data.token)
        return {valid:true, token:resp.data.token}
    }catch(e){
        return false
    }
}