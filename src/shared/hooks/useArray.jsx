import { useState } from 'react';

/**
 * Custom hook to manage array state in React.
 * 
 * @param {Array} [initialArray=[]] - The initial array to start with.
 * @returns {import('../../config/types').UseArray} The state and functions to manipulate the array.
 */
const useArray = (initialArray = []) => {
  const [array, setArray] = useState(initialArray);

  /**
   * Add an element to the end of the array.
   * 
   * @param {*} element - The element to add.
   */
  const push = (element) => {
    setArray((prevArray) => [...prevArray, element]);
  };

  /**
   * Remove an element from the array by its index.
   * 
   * @param {number} index - The index of the element to remove.
   */
  const remove = (index) => {
    setArray((prevArray) => prevArray.filter((_, i) => i !== index));
  };

  /**
   * Update an element in the array by its index.
   * 
   * @param {number} index - The index of the element to update.
   * @param {*} newElement - The new element to replace the old one.
   */
  const update = (index, newElement) => {
    setArray((prevArray) => prevArray.map((el, i) => (i === index ? newElement : el)));
  };

  /**
   * Clear the array.
   */
  const clear = () => {
    setArray([]);
  };

  /**
   * Check if the array is empty.
   * 
   * @returns {boolean} True if the array is empty, false otherwise.
   */
  const isEmpty = () => array.length === 0;

  return {
    array,
    setArray,
    push,
    remove,
    update,
    clear,
    isEmpty,
  };
};

export default useArray;
