import axios from "axios";
import { entornoLogin } from "../../config/config";

function useLogin() {
    const isLogin = async (tokenRecibido) => {
        try {
            const token = localStorage.getItem("token");
            let res = await chkToken(token) || await chkToken(tokenRecibido)
            return res
        } catch (e) {
            return false
        }

    }


    async function chkToken(token) {
        try {
            const resp = await axios.get(entornoLogin, {
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': token
                }
            })
            localStorage.setItem('token', resp.data.token)
            return { valid: true, token: resp.data.token }
        } catch (e) {
            return false
        }
    }
    return { isLogin }
}
export default useLogin;