import axios from "axios";
import { AxiosError } from "axios";
import { useEffect, useState } from "react";

/**
 *
 *
 * @param {{string,string}} {url,token}
 * @return {{data:any,loading:boolean, error:any,setRefetch:func,refetch:number}} 
 */
const useFetch = ({ url, token }) => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [refetch,setRefetch] = useState(0);

    function funcRefetch(){
        setRefetch((prev)=> prev + 1 );
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                setLoading(true);
                const res = await axios.get(url, { headers: { 'Authorization': 'Bearer ' + token } })
                setData(res.data);
                setError(null)
            } catch (error) {
                if (error instanceof AxiosError) {
                    setError(error?.response?.data);
                } else {
                    console.error(error)
                }
            } finally {
                setLoading(false);
            }
        }
        fetchData();
    }, [url, token, refetch])

    return { data, loading, error, setRefetch,refetch,funcRefetch };
};

export default useFetch;
