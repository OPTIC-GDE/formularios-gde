import axios from "axios";
import { AxiosError } from "axios";
import { useState } from "react";

/**
 *
 *
 * @param {{url:string,token:string}} 
 * @return {{ data:any, loading:boolean, error:any, postData:func }} 
 */
const useFetchPost = ({ url, token }) => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const postData = async (value) => {
        try {
            console.log(value)
            setLoading(true);
            const res = await axios.post(url, value,{ headers: { 'Authorization': 'Bearer ' + token } })
            setData(res.data);
            setError(null)
        } catch (error) {
            if (error instanceof AxiosError) {
                setError(error?.response?.data);
            } else {
                console.error(error)
            }
        } finally {
            setLoading(false);
        }
    }

    return { data, loading, error, postData };
};

export default useFetchPost;
