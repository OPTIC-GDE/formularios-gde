import axios from "axios";
import { AxiosError } from "axios";
import { useState } from "react";

/**
 *
 *
 * @param {{url:string,token:string}} 
 * @return {{ data:any, loading:boolean, error:any, deleteData:func }} 
 */
const useDelete = ({ url, token }) => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const deleteData = async (value) => {
        try {
            setLoading(true);
            const res = await axios.delete(url + "/" + encodeURIComponent(value), { headers: { 'Authorization': 'Bearer ' + token } })
            setData(res.data);
            setError(null)
        } catch (error) {
            if (error instanceof AxiosError) {
                setError(error?.response?.data);
            } else {
                console.error(error)
            }
        } finally {
            setLoading(false);
        }
    }

    return { data, loading, error, deleteData };
};

export default useDelete;
