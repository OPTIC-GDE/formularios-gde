export function completaZero(str, chr) {
    return str.substr(str.length - chr, str.length);
}

export function formatFecha(fecha) {
    let fec = new Date(fecha);
    let dia = fec.getDate().toString();
    let mes = (fec.getMonth() + 1).toString();
    let anio = fec.getFullYear().toString();

    return dia + "-" + mes + "-" + anio;
}

export function formatFecha2(fecha) {
    let fec = new Date(fecha);
    return fec.toLocaleString("es-AR");
}
export function validarFecha(fecha) {
    const patron = /^\d{4}-\d{2}-\d{2}$/;
    return patron.test(fecha)
}
export const MENSAJE_ERROR = {
    NO_HAY_EXPEDIENTE: "Tu peticion no encontro expedientes asociados",
    SERVER_ERROR: "Hubo un error para comunicarse con el servidor",
    MAX_CANTIDAD_DATOS: "La cantidad maximo de registros es de 300, en total hay: "
}

export function base64ToUrl(base64, contentType) {
    const byteCharacters = atob(base64);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const pdfBlob = new Blob([byteArray], { type: contentType });
    const pdfUrl = URL.createObjectURL(pdfBlob);
    return pdfUrl;

}