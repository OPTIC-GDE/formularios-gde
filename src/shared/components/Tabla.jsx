import { useState, useEffect } from "react";
import Table from 'react-bootstrap/Table'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import PropTypes from "prop-types";
import Paginacion from "./Paginacion.jsx";

/**
 * @param {ListTablaRender}
 */
function Tabla({ columnsName, data }) {
    const [paginaActiva, setPaginaActiva] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(1);
    const [datosVisibles, setDatosVisibles] = useState([]);

    const ELEMENTOS_POR_PAGINA = 20;

    useEffect(() => {
        setCantidadPaginas(Math.ceil(data.length / ELEMENTOS_POR_PAGINA));
    }, [data]);

    useEffect(() => {
        let primerElemento = (paginaActiva - 1) * ELEMENTOS_POR_PAGINA;
        let ultimoElemento = paginaActiva * ELEMENTOS_POR_PAGINA;
        if (ultimoElemento > data.length) {
            ultimoElemento = data.length;
        }
        const elementosVisiblesAux = [];
        for (let i = primerElemento; i < ultimoElemento; i++) {
            elementosVisiblesAux.push(data[i]);
        }
        setDatosVisibles(elementosVisiblesAux);
    }, [paginaActiva, data]);

    function renderPagination(){
        if (data.length <=  ELEMENTOS_POR_PAGINA) return <></>
        return (
            <Row className="justify-content-md-center">
                <Col md={2}>
                    <Paginacion
                        paginaActiva={paginaActiva}
                        setPaginaActiva={setPaginaActiva}
                        cantidadPaginas={cantidadPaginas}
                    />
                </Col>
            </Row>
        )
    }

    return (
        <>
            {renderPagination()}
            <Row>

                <Table className="tablas-element" responsive="sm">
                    <thead>
                        <tr>
                            {columnsName.map((e, i) => (<th
                                key={i}
                            > {e.nombre}</th>))}
                        </tr>
                    </thead>
                    <tbody>
                        {datosVisibles.map(e => {
                            return (<tr className="tablas-element-row" key={e?.id}>
                                {columnsName.map(c => (<td style={c.style} key={c.nombreAtributo}> {e[c.nombreAtributo]} </td>))}
                            </tr>)
                        })}
                    </tbody>
                </Table>
            </Row>
           {renderPagination()}
        </>

    )


}

Tabla.propTypes = {
    columnsName: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
}


export default Tabla;


/**
 *
 * @typedef {import ("../../config/types.js").ListTablaRender} ListTablaRender
 * @typedef {import ("../../config/types.js").ColumnsTablaRender} ColumnsTablaRender
 */