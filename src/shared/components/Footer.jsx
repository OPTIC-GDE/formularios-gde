import logoURL from '../../assets/logos-footer-buscador.png'
const Footer = () => {
  return (
    <footer className="navbar navbar-expand-lg pie" >
        <img className='mx-auto' src={logoURL }/>
    </footer>
  )
}

export default Footer;
