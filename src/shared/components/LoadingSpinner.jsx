import Spinner from 'react-bootstrap/Spinner'
import PropTypes from 'prop-types'

function LoadingSpinner({size = 'sm'}) {
    return (<Spinner animation="border" size={size} role="status">
        <span className="visually-hidden">Loading...</span>
    </Spinner>)
}


export default LoadingSpinner

LoadingSpinner.propTypes = {
    size: PropTypes.string
}