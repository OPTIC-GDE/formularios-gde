import  Pagination from "react-bootstrap/Pagination";
import PropTypes from 'prop-types'

/**
 * @param {Paginacion} { cantidadPaginas, paginaActiva, setPaginaActiva }
 */
function Paginacion({ cantidadPaginas, paginaActiva, setPaginaActiva }) {
  const CANT_ITEM_PAGINAS_MOSTRAR = 3;

  function onNext() {
    if (paginaActiva < cantidadPaginas) {
      setPaginaActiva(paginaActiva + 1);
    }
  }

  function onPrevious() {
    if (paginaActiva > 1) {
      setPaginaActiva(paginaActiva - 1);
    }
  }

  function onFirst() {
    setPaginaActiva(1);
  }

  function onLast() {
    setPaginaActiva(cantidadPaginas);
  }

  function onGoPage(numberPage) {
    setPaginaActiva(numberPage);
  }

  function createItems() {
    const items = [];
    let ultimaPaginaAMostrar = cantidadPaginas;
    let primeraPaginaAMostrar = paginaActiva;
    if (paginaActiva + CANT_ITEM_PAGINAS_MOSTRAR < cantidadPaginas) {
      ultimaPaginaAMostrar = paginaActiva + 3;
    }

    primeraPaginaAMostrar = paginaActiva - CANT_ITEM_PAGINAS_MOSTRAR;
    if (primeraPaginaAMostrar < 1) {
      primeraPaginaAMostrar = 1;
    }

    for (let number = primeraPaginaAMostrar; number <= ultimaPaginaAMostrar; number++) {
      items.push(
        <Pagination.Item onClick={() => onGoPage(number)} key={number} active={number === paginaActiva}>
          {number}
        </Pagination.Item>
      );
    }
    return items;
  }
  return (
    <Pagination>
      <Pagination.First onClick={onFirst} />
      <Pagination.Prev onClick={onPrevious} />
      {createItems()}
      <Pagination.Next onClick={onNext} />
      <Pagination.Last onClick={onLast} />
    </Pagination>
  );
}

Paginacion.propTypes = {
  cantidadPaginas: PropTypes.number.isRequired,
  paginaActiva: PropTypes.number.isRequired,
  setPaginaActiva: PropTypes.func.isRequired
}

export default Paginacion;

/**
 *
 * @typedef {import ("../../config/types.js").Paginacion} Paginacion
 */