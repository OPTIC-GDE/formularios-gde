import Nav from 'react-bootstrap/Nav';
import logoURL from '../../assets/header-buscador.png'
import '../styles/navbar.css';


const BusquedaNavbar = ({ setRedirect }) => {
  return (
    <Nav className="provincia-nav navbar navbar-expand-lg cabecera" >
      <img className='mx-auto' src={logoURL} />
      <Nav.Item className='justify-content-end'>
        <Nav.Link style={{
          color: 'white'
        }} eventKey={1} onClick={() => { localStorage.setItem('token', ''); setRedirect(true) }} ><u> Logout </u> </Nav.Link>
      </Nav.Item>
    </Nav>)
}

export default BusquedaNavbar;
