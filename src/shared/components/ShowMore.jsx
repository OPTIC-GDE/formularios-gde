import { useState } from "react";
import PropTypes from 'prop-types'
import { Link } from "react-router-dom";

/**
 * 
 * @param {{text:string}} param0 
 * @returns 
 */
function ShowMore({ text }) {
    const [showMore, setShowMore] = useState(false)
    if(text.length < 100) return text
    return (<>
        {showMore ? text : `${text.substring(0, 100)}`}
        <Link className="" onClick={()=>{setShowMore(!showMore)}}>{!showMore ? ' Ver mas' : ' Ver Menos'}</Link>
    </>
    )
}

ShowMore.propTypes = {
    text: PropTypes.string.isRequired,
}

export default ShowMore;