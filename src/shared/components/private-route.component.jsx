import { useEffect, useState } from "react";
import { isLogin } from "../../shared/services/login.service";
import { DIRECCION_LOGIN } from "../../config/config"
import { Outlet, useSearchParams } from 'react-router-dom';
import PropTypes from 'prop-types'



const PrivateRoute = ({redirect, setRedirect}) => {

    const [allow, setAllow] = useState(false);
    const [searchParams, setSearchParams] = useSearchParams();
    const [token, setToken] = useState(localStorage.getItem('token'))

    const logout = () => {
        localStorage.setItem("token",'');
        setRedirect(true);
    }
    
    useEffect(() => {
        const queryParams = searchParams.get("_k")
        isLogin(queryParams)
            .then(({ valid, token }) => {
                setAllow(valid)
                setToken(token)
                setRedirect(!valid)
            })
            .catch(() => {
                setRedirect(true);
            })
            .finally(() => {
                if (queryParams) {
                    searchParams.delete("_k")
                    setSearchParams(searchParams)
                }
            })
    }, [searchParams, setSearchParams,setRedirect])
    if (redirect) return <AbsoluteRedirect to={`${DIRECCION_LOGIN}?redirectTo=${encodeURIComponent(location.origin)}${location.pathname}`} />
    if(!allow && !redirect) return <h1 style={{color:"white"}}>Loading.....</h1>
    if (allow) return <Outlet context={[token, logout]} />
}



function AbsoluteRedirect({ to }) {

    useEffect(() => {
        window.location = to

    }, [to])

    return null

}


AbsoluteRedirect.propTypes = {
    to: PropTypes.string
}


export default PrivateRoute;