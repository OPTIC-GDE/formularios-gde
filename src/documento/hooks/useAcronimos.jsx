import { useState, useEffect } from 'react';
import axios from 'axios'
import { SOLR } from '../../config/config.js';
/**
 * @return { { acronimos:Acronimo[], loading:boolean, error:string | null } } 
 */
function useAcronimos() {
    const [loading, setLoading] = useState(null);
    const [error, setError] = useState(null);
    const [acronimos, setAcronimos] = useState([])

    useEffect(() => {
        setLoading(true)
        setAcronimos(null);
        setError(null);
        const url = SOLR + "/coreGEDO/select?q=*%3A*&rows=3000&fl=tipo_doc_acr%2Ctipo_doc_nombre&wt=json&indent=true&group=true&group.field=tipo_doc_acr&group.main=true";
        axios.get(url).then((res) => {
            setAcronimos(res?.data?.response?.docs)
        })
            .catch((err) => {
                console.error(err)
                setError('Hubo un error al obtener los acronimos')
            }).finally(() => setLoading(false))
    }, [])

    return { acronimos, loading, error }
}

export default useAcronimos;

/** @typedef {import ("../../config/types.js").Acronimo} Acronimo */
