import { useState } from 'react';
import axios from 'axios';
import { SOLR } from '../../config/config'
import { completaZero } from '../../shared/util/helpers';
const NO_DEVOLVIO_DATOS = "Su consulta no devolvio datos!";

/**
 * @return {{ data:Documento[], setData:React.Dispatch<Documento[]>, loading:boolean, error:string, getData:func }} 
 */
const useDocumentos = () => {
	const [data, setData] = useState(null);
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(null);

	const getData = async ({ reparticionFiltro, fechaDesdeFiltro, referenciaFiltro, fechaHastaFiltro, firmanteFiltro, acronimoFiltro, numeroFiltro, anioFiltro, limit }) => {
		setLoading(true);
		setError(null);
		setData(null)
		try {

			const filtroFech = `&fq=fecha_creacion:[${fechaDesdeFiltro}T00:00:00Z TO ${fechaHastaFiltro}T23:59:59Z ]`;
			const filtroRef = `&fq=referencia:*${referenciaFiltro}*`;
			const filtroFirmante = `&fq=usuario_firmante:*${firmanteFiltro}* OR usuario_generador:${firmanteFiltro}`;
			const filtroAcronimo = acronimoFiltro.length == 0 ?
				"" :
				"&fq=tipo_doc_acr:(" + acronimoFiltro.join(" OR ") + ")";
			//const noTribunal = "&fq=-reparticion_generador:DGI%23TSJ";
			const filtroAnio = '&fq=anio_doc:' + anioFiltro;
			let filtroNumero = '&fq=nro_sade:*'
			if(numeroFiltro !== '*' && numeroFiltro !== '' ){
				filtroNumero = '&fq=nro_sade:*' + completaZero('00000000' + numeroFiltro, 8) + '*';
			}

			const docRequest = SOLR + "/coreGEDO/select?" +
				filtroRef +
				filtroFirmante +
				filtroAcronimo +
				filtroFech +
				filtroAnio +
				filtroNumero +
			//	noTribunal +
				"&version=2.2&start=0&rows=" + limit +
				"&indent=on&wt=json" + 
				"&sort=fecha_creacion+desc"
				;
			const res = await axios.get(encodeURI(docRequest), {
				params: {
					q: `reparticion_generador:*${reparticionFiltro}*`
				}
			})
			const response = res.data.response;

			console.log(response)
			let datos = response?.docs;
			console.log(datos)
			if (response.numFound > limit) {
				setError("Su consulta tiene " + response.numFound + " registros " + " la consulta retorna hasta " + limit + " registros")
			}
			if (!datos?.length || datos?.length == 0) {
				setError(NO_DEVOLVIO_DATOS);
				return null
			}
		/*	datos = datos.filter((e) =>
				!e.reparticion_generador.includes("#TSJ") &&
				!e.reparticion_generador.includes("#CM") &&
				e.reparticion_generador !== "TSJ" &&
				e.reparticion_generador !== "CM"
			);
*/
			setData(datos);
		} catch (error) {
			setError(error);
		} finally {
			setLoading(false);
		}
	};

	return { data, setData, loading, error, getData };
};

export default useDocumentos;

/** @typedef {import ("../../config/types.js").Documento} Documento */