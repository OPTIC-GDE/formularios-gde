import { useState } from 'react';
import axios, { AxiosError } from 'axios'
import { DESCARGAR_DOCUMENTO } from '../../config/config.js';
import { base64ToUrl } from '../../shared/util/helpers.js';
/**
 * @return { {descargarDocumento:func, urlDocumento:string, loading:boolean, error:string | null } } 
 */
function useDescargarDocumento() {
    const [loading, setLoading] = useState(null);
    const [error, setError] = useState(null);
    const [urlDocumento, setUrlDocumento] = useState(null)

    const descargarDocumento = (nroSade) => {
        setLoading(true)
        setUrlDocumento(null);
        setError(null);
        const url = DESCARGAR_DOCUMENTO + '/' + encodeURIComponent(nroSade)
        axios.get(url).then((res) => {
            setUrlDocumento(base64ToUrl(res.data.data, "application/pdf"))
        })
            .catch((error) => {
                console.log(error.code)
                if (error instanceof AxiosError && error.response.status == '400') {
                    setError(error.response.data.details)

                } else {
                    setError('Hubo un error al comunicarse con el servidor intentenlo nuevamente mas tarde')
                }
            }).finally(() => setLoading(false))
    }

    return { descargarDocumento, urlDocumento, loading, error, setError }
}

export default useDescargarDocumento;

/** @typedef {import ("../../config/types.js").Acronimo} Acronimo */
