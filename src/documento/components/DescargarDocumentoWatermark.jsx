import { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import LoadingSpinner from "../../shared/components/LoadingSpinner.jsx";
import useDescargarDocumento from "../hooks/useDescargarDocumento";
import Alert from "react-bootstrap/Alert";


function DescargarDocumentoWatermark() {
    const [nroSade, setNroSade] = useState("");
    const [visible, setVisible] = useState(false)
    const [formError, setFormError] = useState(null)
    const { descargarDocumento, error, loading, urlDocumento } = useDescargarDocumento()

    function handleSubmit(e) {
        e.preventDefault();
        console.log(e)
        setFormError(null)
        if (!nroSade) {
            setFormError("Por favor, envíe un número de documento válido.")
            return
        }
        if (!validarNroSade()) return
        descargarDocumento(nroSade);
    }

    function validarNroSade() {
        const arraySade = nroSade.split('-')
        if (arraySade.length < 5) {
            setFormError("Por favor, envíe un codigo de documento válido.")
            return false;
        }
        if (!Number(arraySade[1])) {
            setFormError("Por favor, envíe un anio de documento válido.")
            return false
        }
        if (!Number(arraySade[2])) {
            setFormError("Por favor, envíe un numero de documento válido.")
            return false
        }
        return true
    }
    useEffect(() => {
        if (urlDocumento) setVisible(true)
    }, [urlDocumento])
    return (
        <>

            {!visible ?
                <Container fluid style={{ backgroundColor: '#F4DFB9', padding: '10px', width: "800px" }}>

                    <Form style={{ paddingTop: "15px" }}>
                        <Row>
                            <Form.Group as={Col} controlId="numero">
                                <Form.Label >Número Sade</Form.Label>
                                <Form.Control
                                    name='nroSade'
                                    placeholder="Ej: IF-2024-00005027-NEU-OPTPD#SGP"
                                    onChange={({ target }) => setNroSade(target?.value !== '' ? target.value : "")} />
                            </Form.Group>
                        </Row>
                        <Button
                            variant="primary"
                            type="submit"
                            className='mt-2'
                            size="md"
                            md={2}
                            disabled={loading}
                            onClick={(e) => handleSubmit(e)}
                        >
                            Buscar{" "}
                            {loading ? (<LoadingSpinner />) : null}
                        </Button>
                    </Form>

                </Container>
                :
                <>
                    <i onClick={() => { setVisible(false); setNroSade(null) }} className="volver-atras bi bi-arrow-left-short " style={{ fontSize: '2rem', color: "white" }}></i>
                    <embed src={urlDocumento} id="pdf-embed" width="100%" height="1000em" type="application/pdf" />
                </>

            }
            <Alert className="mt-2" hidden={!error} variant="danger">
                {error}
            </Alert>
            <Alert className="mt-2" hidden={!formError} variant="warning">
                {formError}
            </Alert>
        </>
    )

}

export default DescargarDocumentoWatermark;


