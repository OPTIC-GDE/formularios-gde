import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';
import PropTypes from 'prop-types'
import { formatFecha2 } from '../../shared/util/helpers.js';

/**
 * @param {{contenidoModal:Documento, cerrarModal:Function}} { contenidoModal, cerrarModal }
 * @return {*} 
 */
function DetailModalDocumento({ contenidoModal, cerrarModal }) {
    const { nro_sade,
        referencia,
        usuario_firmante,
        tipo_doc_acr,
        usuario_generador,
        fecha_creacion,
        reparticion_generador,
        tipo_doc_nombre,
        anio_doc } = contenidoModal;

    let usuarioFimante = usuario_firmante ? usuario_firmante.join(", ") : usuario_generador
    if (usuarioFimante.includes('$$$')) {
        usuarioFimante = usuarioFimante.split('$$$').join(', ')
    }
    return (
        <Modal size='lg' show onHide={cerrarModal}>
            <Modal.Header closeButton>
                <Modal.Title>GEDO: {nro_sade}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table size='sm' striped responsive id='tabla-modal'>
                    <tbody>
                        <tr><td width='30%'>AÑO:</td><td>{anio_doc}</td></tr>
                        <tr><td width='30%'>TIPO DOCUMENTO:</td><td>{tipo_doc_acr}</td></tr>
                        <tr><td width='30%'>NOMBRE TIPO DOCUMENTO:</td><td>{tipo_doc_nombre}</td></tr>
                        <tr><td width='30%'>REFERENCIA:</td><td>{referencia}</td></tr>
                        <tr><td width='30%'>USUARIO FIRMANTE:</td><td>{usuarioFimante}</td></tr>
                        <tr><td width='30%'>FECHA CREACION:</td><td>{formatFecha2(fecha_creacion)}</td></tr>
                        <tr><td width='30%'>REPARTICION GENERADOR:</td><td>{reparticion_generador}</td></tr>
                    </tbody>
                </Table>
            </Modal.Body>
        </Modal>)
}



DetailModalDocumento.propTypes = {
    contenidoModal: PropTypes.object.isRequired,
    cerrarModal: PropTypes.func.isRequired
}

/** @typedef {import ("../../config/types.js").Documento} Documento */

export default DetailModalDocumento;
