import { Col, Form } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import useAcronimos from "../hooks/useAcronimos";
import PropTypes from 'prop-types'
import LoadingSpinner from "../../shared/components/LoadingSpinner";


/**
 * 
 * @param {{setAcronimoFiltro:Function, defaultValue: any}} param0 
 */
function ListAcronimosMultivalued({ setAcronimoFiltro, defaultValue }) {
    const { acronimos } = useAcronimos()

    function onCambioTipo(nuevoValor) {
        let valor = defaultValue;
        for (let tipo of nuevoValor) {
            valor.push(tipo?.id);
        }
        setAcronimoFiltro(valor)
    }

    function renderElement() {
        return acronimos.map((resp) => ({
            id: resp.tipo_doc_acr,
            label: `${resp.tipo_doc_acr} - ${resp.tipo_doc_nombre}`
        }));
    }


    return (
        <Form.Group as={Col} controlId="tipo">
            <Form.Label >Tipo de GEDO</Form.Label>
            {!acronimos ? (
                <LoadingSpinner />
            ) :
                <Typeahead
                    multiple
                    id="typeahead"
                    options={renderElement()}
                    placeholder="Ej: ACDO - CUERDO, IFI - informe Importado"
                    onChange={(e) => onCambioTipo(e)}
                />}
        </Form.Group>)

}
ListAcronimosMultivalued.propTypes = {
    setAcronimoFiltro: PropTypes.func.isRequired,
    defaultValue: PropTypes.array.isRequired
}

export default ListAcronimosMultivalued;