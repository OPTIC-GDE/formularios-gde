import { useState } from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import Row from "react-bootstrap/Row";
import { formatFecha2, validarFecha } from "../../shared/util/helpers";
import ListarReparticionesInputBox from "../../reparticiones/components/ListReparticiones";
import ListAcronimosMultivalued from "./ListAcronimosMultivalued";
import Tabla from "../../shared/components/Tabla";
import useDocumentos from "../hooks/useDocumentos.jsx";
import LoadingSpinner from "../../shared/components/LoadingSpinner.jsx";
import DetailModalDocumento from "./DetalleDocumento.jsx";
import ShowMore from "../../shared/components/ShowMore.jsx";


/** @type {ColumnsTablaRender[]} */
const columnasConfig = [{
	nombreAtributo: 'nro_sade',
	nombre: 'GEDO',
	style: {
		width: '25%',
		cursor: 'pointer'
	}
}, {
	nombreAtributo: 'referencia',
	nombre: 'Referencia'
}, {
	nombreAtributo: 'usuario_generador',
	nombre: 'Usuario Firmante'
}, {
	nombreAtributo: 'reparticion_generador',
	nombre: 'Repartición'
}, {
	nombreAtributo: 'fecha_creacion',
	nombre: 'Fecha Creación',
}]

const FindDocumento = () => {

	const DEFAULT_VALUES = {
		NUMEROS_REGISTROS: 7000,
		REPARTICION: '*',
		REFERENCIA: '*',
		FIRMANTE: '*',
		ANIO: '*',
		NUMERO: '*',
		FECHA_DESDE: "2016-12-01",
		FECHA_HASTA: "2040-12-01",
		ERROR_MESSAGE: '',
		ACRONIMOS: []
	}
	const TITULO = "Busca documentos de acuerdo a los filtros que se ingresen";

	const [reparticionFiltro, setReparticionFiltro] = useState(DEFAULT_VALUES.REPARTICION);
	const [numeroFiltro, setNumeroFiltro] = useState(DEFAULT_VALUES.NUMERO);
	const [anioFiltro, setAnioFiltro] = useState(DEFAULT_VALUES.ANIO);
	const [referenciaFiltro, setReferenciaFiltro] = useState(DEFAULT_VALUES.REFERENCIA);
	const [firmanteFiltro, setFirmanteFiltro] = useState(DEFAULT_VALUES.FIRMANTE);
	const [fechaDesdeFiltro, setFechaDesdeFiltro] = useState(DEFAULT_VALUES.FECHA_DESDE);
	const [fechaHastaFiltro, setFechaHastaFiltro] = useState(DEFAULT_VALUES.FECHA_HASTA);
	const [acronimoFiltro, setAcronimoFiltro] = useState(DEFAULT_VALUES.ACRONIMOS);
	const [errorMessage, setErrorMessage] = useState(DEFAULT_VALUES.ERROR_MESSAGE);
	const [mostrarDetalle, setMostrarDetalle] = useState(false);
	const [detalle, setDetalle] = useState(null);

	const { data: documentos, error: errorFetching, getData: getDocumentos, loading } = useDocumentos();

	const onBuscarHandler = (e) => {
		e.preventDefault();
		setErrorMessage('');
		getDocumentos({
			reparticionFiltro,
			acronimoFiltro,
			fechaDesdeFiltro,
			fechaHastaFiltro,
			firmanteFiltro,
			referenciaFiltro,
			numeroFiltro,
			anioFiltro,
			limit: DEFAULT_VALUES.NUMEROS_REGISTROS
		})
	}


	function formatearFilas() {
		return documentos.map(doc => {
			let usuarioGenerador = doc.usuario_firmante ? doc.usuario_firmante.join(', ') : doc.usuario_generador;
			if(usuarioGenerador.includes('$$$')){
				usuarioGenerador = usuarioGenerador.split('$$$').join(', ')
			}
			return {
				...doc,
				id: doc.nro_sade,
				usuario_generador:usuarioGenerador,
				fecha_creacion: formatFecha2(doc.fecha_creacion),
				nro_sade: <div className='tablas-element-row' onClick={() => {
					setDetalle(doc);
					setMostrarDetalle(true);
				}}>{doc.nro_sade}</div>,
				referencia: <ShowMore text={doc.referencia} />
			}
		})
	}
	return (
		<>

			{mostrarDetalle ?
				<DetailModalDocumento
					contenidoModal={detalle}
					mostrarModal={mostrarDetalle}
					cerrarModal={() => setMostrarDetalle(false)}
				/> : <></>}
			<Container fluid style={{ backgroundColor: '#e2e2e2', paddingTop: '10px', width: "800px" }}>

				<h2 style={{ color: "blue" }}> {TITULO} </h2>
				<Form style={{ paddingTop: "15px" }}>
					<Row>
						<Form.Group as={Col} controlId="numero">
							<Form.Label >Número documento</Form.Label>
							<Form.Control
								name='numero'
								placeholder="Ej: 35"
								onChange={({ target }) => setNumeroFiltro(target?.value !== '' ? target.value : DEFAULT_VALUES.NUMERO)} />
						</Form.Group>

						<Form.Group as={Col} controlId="anio">
							<Form.Label >Año del documento</Form.Label>
							<Form.Control
								name='anio'
								placeholder="Ej: 2024"
								onChange={({ target }) => setAnioFiltro(target?.value !== '' ? target.value : DEFAULT_VALUES.ANIO)} />
						</Form.Group>
					</Row>
					<Row>
						<Form.Group controlId="referencia">
							<Form.Label >Referencia</Form.Label>
							<Form.Control
								placeholder="Ej: Ingrese referencia de un documento a buscar..."
								onChange={({ target }) => setReferenciaFiltro(target?.value !== '' ? target.value : DEFAULT_VALUES.REFERENCIA)}
							/>
						</Form.Group>
					</Row>
					<Row>
						<ListarReparticionesInputBox
							defaultValue={DEFAULT_VALUES.REPARTICION}
							setFiltroReparticion={setReparticionFiltro}
						/>
					</Row>
					<Row>
						<ListAcronimosMultivalued
							defaultValue={DEFAULT_VALUES.ACRONIMOS}
							setAcronimoFiltro={setAcronimoFiltro}
						/>

						<Form.Group as={Col} controlId="usuario">
							<Form.Label >
								Usuario Firmante
							</Form.Label>
							<Form.Control
								placeholder="Ej: MGIGLIO"
								onChange={({ target }) => setFirmanteFiltro(target?.value !== '' ? target.value : DEFAULT_VALUES.FIRMANTE)}
							/>
						</Form.Group>
					</Row>
					<Row>
						<Form.Group as={Col} controlId="fechaDesde">
							<Form.Label>Fecha Desde</Form.Label>
							<Form.Control
								type="date"
								placeholder="Fecha Desde..."
								onChange={(e) => {
									const nuevaFecha = validarFecha(e?.target?.value) ? e.target.value : DEFAULT_VALUES.FECHA_DESDE;
									setFechaDesdeFiltro(nuevaFecha)
								}}
							/>
						</Form.Group>

						<Form.Group as={Col} controlId="fechaHasta">
							<Form.Label >Fecha Hasta</Form.Label>
							<Form.Control
								type="date"
								placeholder="Fecha Hasta..."
								onChange={(e) => {
									const nuevaFecha = validarFecha(e?.target?.value) ? e.target.value : DEFAULT_VALUES.FECHA_HASTA;
									setFechaHastaFiltro(nuevaFecha)
								}}
							/>
						</Form.Group>
					</Row>
					<Button
						variant="primary"
						type="submit"
						className='mt-2'
						size="md"
						md={2}
						disabled={loading}
						onClick={onBuscarHandler}
					>
						Buscar{" "}
						{loading ? (<LoadingSpinner />) : null}
					</Button>
					<hr />
				</Form>
			</Container>
			<Alert hidden={!errorFetching} variant="warning">
				{errorFetching}
			</Alert>

			<Alert hidden={!errorMessage} variant="warning">
				{errorMessage}
			</Alert>
			{documentos ? <Tabla data={formatearFilas()} columnsName={columnasConfig} /> : <></>}
		</>
	);

}

export default FindDocumento;

/** @typedef {import ("../../config/types.js").ColumnsTablaRender} ColumnsTablaRender */