import Container from "react-bootstrap/Container";
import { useOutletContext } from "react-router-dom";
import useFetch from "../../shared/hooks/useFetch.jsx";
import { ADD_CONFIG_URL, ADD_CONFIG_USER, DELETE_CONFIG_URL, DELETE_CONFIG_USER, GET_CONFIG } from "../../config/config.js";
import Tabla from "../../shared/components/Tabla.jsx";
import LoadingSpinner from "../../shared/components/LoadingSpinner.jsx";
import DeleteIcon from "../../assets/delete-icon.svg"
import { Button } from "react-bootstrap";
import { useState } from "react";
import ModalConfig from "./ModalConfig.jsx";
import useFetchPost from "../../shared/hooks/useFetchPost.jsx";

/** @type {ColumnsTablaRender[]} */
const columnasConfigUsuarios = [{
    style: {
        width: '80%'
    },
    nombreAtributo: 'username',
    nombre: 'Usuarios validos',
}, {
    nombreAtributo: 'accion',
    nombre: 'Eliminar'
}]

const columnasConfigUrl = [{
    style: {
        width: '80%'
    },
    nombreAtributo: 'url',
    nombre: 'url validas',
}, {
    nombreAtributo: 'accion',
    nombre: 'Eliminar'
}
]
function ConfigUrlUsuario() {
    const [token] = useOutletContext();
    const [tituloModal, setTituloModal] = useState('')
    const [showModal, setShowModal] = useState(false)
    const [accionModal, setAccionModal] = useState('')

    // El uso de doble ()=> ()=> es necesario para poder setear funciones en los estados
    const [onConfirmModal, setOnConfirmModal] = useState(() => () => { })
    const { data, loading, refetch, setRefetch } = useFetch({ url: GET_CONFIG, token });

    const { postData: deleteUrl } = useFetchPost({ url: DELETE_CONFIG_URL, token });
    const { postData: deleteUser } = useFetchPost({ url: DELETE_CONFIG_USER, token });

    const { postData: addUser } = useFetchPost({ url: ADD_CONFIG_USER, token });
    const { postData: addUrl } = useFetchPost({ url: ADD_CONFIG_URL, token });
    
    function formatUsuarios() {
        return data.validUsers.map(e => ({
            username: e, accion: (<img onClick={() => {
                setAccionModal('delete');
                setTituloModal('Esta seguro que desea eliminar el usuario ' + e + '?')
                setOnConfirmModal(() => () => {
                    deleteUser({ username: e }).then(() => setRefetch(refetch + 1))
                })
                setShowModal(true);
            }} src={DeleteIcon} />)
        }))
    }

    function formatUrl() {
        return data.validUrl.map(e => ({
            url: e, accion: (<img onClick={() => {
                setAccionModal('delete');
                setTituloModal('Esta seguro que desea eliminar la url ' + e + '?')
                setOnConfirmModal(() => () => { deleteUrl({ url: e }).then(() => setRefetch(refetch + 1)) })
                setShowModal(true);
            }} src={DeleteIcon} />)
        }))
    }

    if (loading) return <LoadingSpinner />
    if (!data) return
    return (<>
        <ModalConfig
            show={showModal}
            handleClose={() => setShowModal(false)}
            accion={accionModal}
            titulo={tituloModal}
            onConfirm={onConfirmModal} />
        <Container className="shadow-lg" style={{ backgroundColor: '#e2e2e2', width: "800px" }}>

            <h4 style={{ color: 'blue' }}>Whitelist de usuarios, si esta vacia significa que esta habilitado para todos</h4>
            <Tabla
                columnsName={columnasConfigUsuarios}
                data={formatUsuarios()}

            />
            <Button
                variant="success"
                className='m-2'
                size="md"
                md={2}
                onClick={() => {
                    setAccionModal('agregar');
                    setTituloModal('Agregar usuario')
                    setOnConfirmModal(() => (newConfig) => { addUser({ username: newConfig }).then(() => setRefetch(refetch + 1)) })
                    setShowModal(true);
                }}
            >
                Agregar usuario{" "}
            </Button>
            <h4 style={{ color: 'blue' }}>Whitelist de url que puede redirigir el login</h4>

            <Tabla
                columnsName={columnasConfigUrl}
                data={formatUrl()}

            />

            <Button
                variant="success"
                className='m-2'
                size="md"
                md={2}
                onClick={() => {
                    setAccionModal('agregar');
                    setTituloModal('Agregar URL')
                    setOnConfirmModal(() => (newConfig) => { addUrl({ url: newConfig }).then(() => setRefetch(refetch + 1)) })
                    setShowModal(true);
                }}
            >
                Agregar url{" "}
            </Button>


        </Container>
        <Container style={{ paddingTop: "15px" }}>

        </Container>
    </>)
}

export default ConfigUrlUsuario;

/** @typedef {import ("../../config/types.js").ColumnsTablaRender} ColumnsTablaRender */
