import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import PropTypes from 'prop-types'
import { useState } from 'react';

function ModalConfig({ titulo, show, handleClose, accion, onConfirm }) {
    const [newConfig, setNewConfig] = useState('')
    function confirm() {
        handleClose();
        onConfirm(newConfig);
    }
    return (
        <>
            <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>{titulo}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {accion === 'agregar' ? (<Row>
                        <Form.Group as={Col} xs controlId="param">
                            <Form.Label ></Form.Label>
                            <Form.Control
                                name='params'
                                placeholder=""
                                onChange={(e) => { setNewConfig(e?.target?.value) }} />
                        </Form.Group>
                    </Row>) : <></>}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="success" onClick={() => confirm()}>
                        Confirmar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}


ModalConfig.propTypes = {
    titulo: PropTypes.string.isRequired,
    show: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    accion: PropTypes.string.isRequired,
    onConfirm: PropTypes.func.isRequired
}



export default ModalConfig