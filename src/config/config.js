const URL = import.meta.env.VITE_URL_SERVICE;
const AMBIENTE = import.meta.env.VITE_AMBIENTE;
const URL_API_REDIS = URL + '/consultas';

export const GET_CONFIG = URL + "/api/login/config"
export const DELETE_CONFIG_USER = URL + "/api/login/config/deleteUser"
export const DELETE_CONFIG_URL = URL + "/api/login/config/deleteUrl"
export const ADD_CONFIG_USER = URL + "/api/login/config/user"
export const ADD_CONFIG_URL = URL + "/api/login/config/url"
export const DESCARGAR_DOCUMENTO = URL_API_REDIS + '/descargarDocumento'

export const URL_OBTENER_COMUNICADOS = URL_API_REDIS + '/obtenerComunicadosUsuario/' + AMBIENTE;
export const URL_NOTIFICAR_COMUNICADOS = URL_API_REDIS + '/comunicarUsuariosExternos/' + AMBIENTE;
export const DIRECCION_LOGIN = URL + "/login/#"
export const IS_LOGIN = URL + "/api/login/isLogin"
export const SOLR = `${URL}/copia/bweb-solr`
export const direccionLogin = `${URL}/login/#/`;
export const urlExpedientesActivos = `${URL}/consultas/obtenerExpedientesActivos`



