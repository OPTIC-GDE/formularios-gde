/** 
 * @typedef {Object} Paginacion
 * @property {number} cantidadPaginas - cantidad de paginas
 * @property {number} paginaActiva - indicar pagina activa
 * @property {Function} setPaginaActiva - funcion para cambiar la pagina activa
 */

/**
 * @typedef {Object} ListTablaRender
 * @property {any[]} data - Lista de elementos a renderizar
 * @property {ColumnsTablaRender[]} columnsName - Lista de columnas a renderizar
 */

/**
 * @typedef {Object} ColumnsTablaRender
 * @property {string} nombre - Nombre de columna a renderizar
 * @property {object} style - Nombre de columna a renderizar
 * @property {string} nombreAtributo - atributo del la lista de elementos a renderizar
 * @property {string} ancho - ancho del la columna para ese elemento en %
 */

/**
* @typedef {object} Reparticion
* @property {string} codigo
* @property {number} id
* @property {string} nombre
*/

/**
* @typedef {object} TypeaheadElement
* @property {string} id
* @property {string} label
*/

/**
* @typedef {object} DetalleModalExpediente
* @property {Expediente} contenidoModal 
* @property {boolean} mostrarModal
* @property {Func} cerrarModal
*/

/**
* @typedef {object} Trata
* @property {string} trata_descripcion
* @property {string} codigo_trata
*/

/**
* @typedef {object} Acronimo
* @property {string} tipo_doc_acr
* @property {string} tipo_doc_nombre
*/

/**
* @typedef {object} HistorialExpediente
* @property {string} asignado
* @property {string} motivo
* @property {string} fechaOperacion
* @property {string} reparticionDestino
*/

/**
* @typedef {object} Expediente
* @property {string} fecha_creacion
* @property {string} descripcion
* @property {string} anio
* @property {string} codigo_reparticion_actuacion
* @property {string} estado
* @property {string} usuario_creador
* @property {string} id_trata
* @property {string} sistema_creador
* @property {string} numero
* @property {string} tipo_documento
* @property {string} codigo_reparticion_usuario
* @property {string} id
* @property {string} id_workflow
* @property {string[]} fecha_operacion
* @property {string[]} assignee
* @property {string[]} motivo_historial
* @property {string[]} codigo_reparticion_destino
* @property {string} EMAIL
* @property {string} id_documento
* @property {string} ID_DOCUMENTO
* @property {string} ID_SOLICITANTE
* @property {string} trata_descripcion
* @property {string} INTERNA
* @property {string} codigo_trata
* @property {string} motivo
* @property {string} MOTIVO_INTERNO
* @property {string} MOTIVO_EXTERNO
* @property {string} _version_
*/

/**
 * @typedef {Object} Documento
 * @property {string} nro_sade - Número SADE del documento.
 * @property {string} referencia - Referencia del documento.
 * @property {string[]} usuario_firmante - Usuario que firma el documento se usa cuando son multiples firmantes.
 * @property {string} tipo_doc_acr - Acrónimo del tipo de documento.
 * @property {string} usuario_generador - Usuario que genera el documento.
 * @property {string} fecha_creacion - Fecha de creación del documento.
 * @property {string} reparticion_generador - Repartición que genera el documento.
 * @property {string} tipo_doc_nombre - Nombre del tipo de documento.
 * @property {number} anio_doc - Año del documento.
 */

/**
* @typedef {object} TablaElementoExpediente
* @property {string} expediente
* @property {string} id_documento
* @property {string} motivo
* @property {string} usuario_creador
* @property {string} assignee_ee
* @property {string} fecha_creacion
 */

/**
 * @typedef {Object} UseArray
 * @property {Array} array - The current state of the array.
 * @property {Function} setArray - Function to set the state of the array.
 * @property {Function} push - Function to add an element to the array.
 * @property {Function} remove - Function to remove an element from the array by index.
 * @property {Function} update - Function to update an element in the array by index.
 * @property {Function} clear - Function to clear the array.
 * @property {Function} isEmpty - Function to check if the array is empty.
 */

/**
 * @typedef {Object} Destinatario
 * @property  {number} id 
 * @property  {string} reparticionDestino 
 * @property  {string} mesaExterna 
 * @property  {string} nroSade 
 * @property  {boolean} enviar 
 * @property  {string} leido       
 * @property  {string} nombreUsuario       
*/


export const unused = {};